$(document).on('click', 'a.btn-danger', function(e) {
    var doDelete = confirm("Are you sure you want to delete this entry?");
    if(!doDelete) {
        e.preventDefault();
    }
});

$(document).on('click', 'a.report', function(e) {
    e.preventDefault();
    location.href = $(this).attr('href') + '?start=' + $('#startdate').val() + '&end=' + $('#enddate').val();
});