<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

use App\Http\Requests;

class TransactionTypesController extends Controller
{
    public function getTransactionTypes() {
        $user = Auth::user();

        return DB::table('transactiontypes')
            ->where('transactiontypes.users_id', '=', $user->id)
            ->orderByRaw('coalesce(transactiontypes.ordering, 9999)')
            ->orderBy('transactiontypes.transactiontype_name')
            ->get();
    }

    public function getTransactionType($id) {
        $types = DB::table('transactiontypes')
            ->where('transactiontypes.transactiontype_id', '=', $id)
            ->orderByRaw('coalesce(transactiontypes.ordering, 9999)')
            ->orderBy('transactiontypes.transactiontype_name')
            ->get();

        if(count($types) > 0) {
            return $types[0];
        } else {
            return null;
        }
    }

    public function getTransactionTypesByParent($parent_id) {
        $user = Auth::user();

        return DB::table('transactiontypes')
            ->where('transactiontypes.users_id', '=', $user->id)
            ->where('transactiontypes.transactiontype_parent_id', '=', $parent_id)
            ->orderByRaw('coalesce(transactiontypes.ordering, 9999)')
            ->orderBy('transactiontypes.transactiontype_name')
            ->get();
    }

    public function getParentTransactionTypes() {
        $user = Auth::user();

        return DB::table('transactiontypes')
            ->where('transactiontypes.users_id', '=', $user->id)
            ->whereNull('transactiontypes.transactiontype_parent_id')
            ->orderByRaw('coalesce(transactiontypes.ordering, 9999)')
            ->orderBy('transactiontypes.transactiontype_name')
            ->get();
    }

    public function getTransactionTypesBySearch($search_string) {
        $user = Auth::user();

        $types = DB::table('transactiontypes')
            ->join('transactiontype_map', 'transactiontype_map.transactiontype_id', '=', 'transactiontypes.transactiontype_id')
            ->where('transactiontypes.users_id', '=', $user->id)
            ->orderByRaw('coalesce(transactiontypes.ordering, 9999)')
            ->orderBy('transactiontypes.transactiontype_name')
            ->get();

        $ttypes = array();
        foreach($types as $type) {
            if(stripos($search_string, $type->map_search) !== false) {
                $ttypes[] = $type;
            }
        }

        return $ttypes;
    }

    //TODO: This works great for three levels, but no doubt someone will want 4,5,6,20, etc.
    public function getNestedTransactionTypes() {
        $typearray = array();

        $types = $this->getParentTransactionTypes();
        foreach($types as $type) {
            $typearray[] = $type;
            //echo '<pre>'; var_dump($type); echo '</pre>';
            foreach($this->getTransactionTypesByParent($type->transactiontype_id) as $typechild) {
                $typechild->transactiontype_name = '-'.$typechild->transactiontype_name;
                $typearray[] = $typechild;
                //echo '<pre>'; var_dump($typechild); echo '</pre>';
                foreach($this->getTransactionTypesByParent($typechild->transactiontype_id) as $typegrandchild) {
                    $typegrandchild->transactiontype_name = '--'.$typegrandchild->transactiontype_name;
                    $typearray[] = $typegrandchild;
                    //echo '<pre>'; var_dump($typegrandchild); echo '</pre>';
                }
            }
        }

        return $typearray;
    }

    public function getList() {
        $transactiontypes = $this->getNestedTransactionTypes();
        return view('transactiontypes.list', ['transactiontypes' => $transactiontypes]);
    }

    public function addForm() {
        $types = $this->getNestedTransactionTypes();
        return view('transactiontypes.add', ['types' => $types]);
    }

    public function addFormSave(Request $request) {
        $user = Auth::user();

        if($request->transactiontype_parent_id > 0) {
            $parent = $request->transactiontype_parent_id;
        } else {
            $parent = null;
        }

        $transactiontype_id = DB::table('transactiontypes')
            ->insertGetId([
                'users_id' => $user->id,
                'transactiontype_name' => $request->transactiontype_name,
                'transactiontype_colour' => $request->transactiontype_colour,
                'transactiontype_parent_id' => $parent
            ]);

        if(isset($request->map_id)) {
            foreach ($request->map_id as $idx => $map_id) {
                $map_search = $request->map_search[$idx];

                DB::table('transactiontype_map')
                    ->insert([
                        'transactiontype_id' => $transactiontype_id,
                        'map_search' => $map_search
                    ]);
            }
        }

        return redirect('/transactiontypes')->with('success', 'Your transaction type has been added.');
    }

    public function editForm($transactiontype_id) {
        $transactiontypes = DB::table('transactiontypes')
            ->where('transactiontype_id', '=', $transactiontype_id)
            ->get();

        if(count($transactiontypes) == 0) {
            return view('errors.404', ['message' => 'The transaction type you\'re looking for cannot be found.']);
        } else {
            $transactiontype = $transactiontypes[0];
            $maps = DB::table('transactiontype_map')
                ->where('transactiontype_id', '=', $transactiontype_id)
                ->get();
            $types = $this->getNestedTransactionTypes();
            return view('transactiontypes.edit', ['transactiontype' => $transactiontype, 'maps' => $maps, 'types' => $types]);
        }
    }

    public function editFormSave(Request $request) {
        if($request->transactiontype_parent_id > 0) {
            $parent = $request->transactiontype_parent_id;
        } else {
            $parent = null;
        }

        DB::table('transactiontypes')
            ->where('transactiontype_id', '=', $request->transactiontype_id)
            ->update([
                'transactiontype_name' => $request->transactiontype_name,
                'transactiontype_colour' => $request->transactiontype_colour,
                'transactiontype_parent_id' => $parent
            ]);

        //TODO: process deleted maps

        //process added or updated maps
        if(isset($request->map_id)) {
            foreach ($request->map_id as $idx => $map_id) {
                $map_search = $request->map_search[$idx];

                if ($map_id > 0) {
                    DB::table('transactiontype_map')
                        ->where('map_id', '=', $map_id)
                        ->update([
                            'map_search' => $map_search
                        ]);
                } else {
                    DB::table('transactiontype_map')
                        ->insert([
                            'transactiontype_id' => $request->transactiontype_id,
                            'map_search' => $map_search
                        ]);
                }
            }
        }

        return redirect('/transactiontypes')->with('success', 'Your transaction type was updated successfully');
    }

    public function delete($transactiontype_id) {
        DB::table('transactiontype_map')
            ->where('transactiontype_id', '=', $transactiontype_id)
            ->delete();

        DB::table('transactiontypes')
            ->where('transactiontype_id', '=', $transactiontype_id)
            ->delete();

        return redirect('/transactiontypes')->with('success', 'The transaction type has been deleted successfully.');
    }
}
