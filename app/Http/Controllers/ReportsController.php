<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

use App\Http\Requests;

class ReportsController extends Controller
{
    public function showList() {
        return view('reports.list');
    }

    public function transactionsByType() {
        $ttc = new TransactionTypesController();
        if(isset($_GET['startdate']) && isset($_GET['enddate'])) {
            $startdate = date('Y-m-d', strtotime($_GET['start']));
            $enddate = date('Y-m-d H:i:s', strtotime($_GET['end']));
        } else {
            if((int)date('m') < 9) {
                //last FY
                $startdate = ((int)date('Y') - 1).'-07-01';
                $enddate = ((int)date('Y')).'-06-30';
            } else {
                //this FY
                $startdate = ((int)date('Y')).'-07-01';
                $enddate = ((int)date('Y') + 1).'-06-30';
            }
        }
        $txns = array();

        $topLevel = $ttc->getParentTransactionTypes();
        foreach($topLevel as $type) {
            //echo $type->transactiontype_name.'<br />';
            $amount = 0;
            $newtxns = DB::table('transactions')
                //->where('transactions.account_from_id', '>', 0)
                ->where('transactions.transactiontype_id', '=', $type->transactiontype_id)
                ->where('transactions.transaction_date', '>=', $startdate)
                ->where('transactions.transaction_date', '<=', $enddate)
                //->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                ->select('transactions.transaction_amount', 'transactions.transaction_memo', 'transactions.account_from_id')
                ->get();
            if(count($newtxns) > 0) {
                foreach($newtxns as $newtxn) {
                    //echo $newtxn->transaction_memo . ': ' . $newtxn->transaction_amount . '<br />';
                    $amount += ($newtxn->transaction_amount * ($newtxn->account_from_id > 0 ? -1 : 1));
                }
            }

            $midLevel = $ttc->getTransactionTypesByParent($type->transactiontype_id);
            foreach($midLevel as $childType) {
                //echo $childType->transactiontype_name.'<br />';
                $newtxns = DB::table('transactions')
                    //->where('transactions.account_from_id', '>', 0)
                    ->where('transactions.transactiontype_id', '=', $childType->transactiontype_id)
                    ->where('transactions.transaction_date', '>=', $startdate)
                    ->where('transactions.transaction_date', '<=', $enddate)
                    //->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                    ->select('transactions.transaction_amount', 'transactions.transaction_memo', 'transactions.account_from_id')
                    ->get();
                if(count($newtxns) > 0) {
                    foreach($newtxns as $newtxn) {
                        //echo $newtxn->transaction_memo . ': ' . $newtxn->transaction_amount . '<br />';
                        $amount += ($newtxn->transaction_amount * ($newtxn->account_from_id > 0 ? -1 : 1));
                    }
                }

                $lowLevel = $ttc->getTransactionTypesByParent($childType->transactiontype_id);

                foreach($lowLevel as $grandChildType) {
                    //echo $grandChildType->transactiontype_name.' ('.$grandChildType->transactiontype_id.')<br />';
                    $newtxns = DB::table('transactions')
                        //->where('transactions.account_from_id', '>', 0)
                        ->where('transactions.transactiontype_id', '=', $grandChildType->transactiontype_id)
                        ->where('transactions.transaction_date', '>=', $startdate)
                        ->where('transactions.transaction_date', '<=', $enddate)
                        //->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                        ->select('transactions.transaction_amount', 'transactions.transaction_memo', 'transactions.account_from_id')
                        ->get();
                    if(count($newtxns) > 0) {
                        foreach($newtxns as $newtxn) {
                            //echo $newtxn->transaction_memo . ': ' . $newtxn->transaction_amount . '<br />';
                            $amount += ($newtxn->transaction_amount * ($newtxn->account_from_id > 0 ? -1 : 1));
                        }
                    }
                }
            }

            $txns[] = (object)array(
                'transactiontype_colour' => $type->transactiontype_colour,
                'transactiontype_name' => $type->transactiontype_name,
                'transactiontype_id' => $type->transactiontype_id,
                'transaction_amount' => $amount
            );
        }

        $amount = 0;
        $newtxns = DB::table('transactions')
            ->where('transactions.account_from_id', '>', 0)
            ->whereNull('transactions.transactiontype_id')
            ->where('transactions.transaction_date', '>=', $startdate)
            ->where('transactions.transaction_date', '<=', $enddate)
            ->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
            ->get();
        if(count($newtxns) > 0) {
            $amount += $newtxns[0]->transaction_amount;
        }

        $txns[] = (object)array(
            'transactiontype_colour' => '#000000',
            'transactiontype_name' => 'Other',
            'transactiontype_id' => 0,
            'transaction_amount' => $amount
        );


        return view('reports.transactiontype', ['transactions' => $txns, 'title' => 'All transactions']);
    }

    public function transactionsListByType() {
        $content = '<table class="table table-striped">';
        $ttc = new TransactionTypesController();
        if(isset($_GET['startdate']) && isset($_GET['enddate'])) {
            $startdate = date('Y-m-d', strtotime($_GET['start']));
            $enddate = date('Y-m-d H:i:s', strtotime($_GET['end']));
        } else {
            if((int)date('m') < 9) {
                //last FY
                $startdate = ((int)date('Y') - 1).'-07-01';
                $enddate = ((int)date('Y')).'-06-30';
            } else {
                //this FY
                $startdate = ((int)date('Y')).'-07-01';
                $enddate = ((int)date('Y') + 1).'-06-30';
            }
        }
        $txns = array();

        $topLevel = $ttc->getParentTransactionTypes();
        foreach($topLevel as $type) {
            $content.= '<tr><td colspan="3"><h1>'.$type->transactiontype_name.'</h1></td></tr>';
            $amount = 0;
            $newtxns = DB::table('transactions')
                //->where('transactions.account_from_id', '>', 0)
                ->where('transactions.transactiontype_id', '=', $type->transactiontype_id)
                ->where('transactions.transaction_date', '>=', $startdate)
                ->where('transactions.transaction_date', '<=', $enddate)
                //->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                ->select('transactions.transaction_amount', 'transactions.transaction_memo', 'transactions.transaction_date', 'transactions.account_from_id', 'transactions.account_to_id')
                ->orderBy('transactions.transaction_date')
                ->get();
            if(count($newtxns) > 0) {
                foreach($newtxns as $newtxn) {
                    $content.= '<tr><td>'.$newtxn->transaction_date.'</td><td>'.$newtxn->transaction_memo . '</td><td>' . ($newtxn->account_from_id > 0 ? '-' : '').$newtxn->transaction_amount . '</td></tr>';
                    $amount += ($newtxn->transaction_amount * ($newtxn->account_from_id > 0 ? -1 : 1));
                }
            }

            $midLevel = $ttc->getTransactionTypesByParent($type->transactiontype_id);
            foreach($midLevel as $childType) {
                $content.= '<tr><td colspan="3"><h2>'.$childType->transactiontype_name.'</h2></td></tr>';
                $newtxns = DB::table('transactions')
                    //->where('transactions.account_from_id', '>', 0)
                    ->where('transactions.transactiontype_id', '=', $childType->transactiontype_id)
                    ->where('transactions.transaction_date', '>=', $startdate)
                    ->where('transactions.transaction_date', '<=', $enddate)
                    //->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                    ->select('transactions.transaction_amount', 'transactions.transaction_memo', 'transactions.transaction_date', 'transactions.account_from_id', 'transactions.account_to_id')
                    ->orderBy('transactions.transaction_date')
                    ->get();
                if(count($newtxns) > 0) {
                    foreach($newtxns as $newtxn) {
                        $content.= '<tr><td>'.$newtxn->transaction_date.'</td><td>'.$newtxn->transaction_memo . '</td><td>' . ($newtxn->account_from_id > 0 ? '-' : '').$newtxn->transaction_amount . '</td></tr>';
                        $amount += ($newtxn->transaction_amount * ($newtxn->account_from_id > 0 ? -1 : 1));
                    }
                }

                $lowLevel = $ttc->getTransactionTypesByParent($childType->transactiontype_id);

                foreach($lowLevel as $grandChildType) {
                    $content.= '<tr><td colspan="3"><h3>'.$grandChildType->transactiontype_name.'</h3></td></tr>';
                    $newtxns = DB::table('transactions')
                        //->where('transactions.account_from_id', '>', 0)
                        ->where('transactions.transactiontype_id', '=', $grandChildType->transactiontype_id)
                        ->where('transactions.transaction_date', '>=', $startdate)
                        ->where('transactions.transaction_date', '<=', $enddate)
                        //->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                        ->select('transactions.transaction_amount', 'transactions.transaction_memo', 'transactions.transaction_date', 'transactions.account_from_id', 'transactions.account_to_id')
                        ->orderBy('transactions.transaction_date')
                        ->get();
                    if(count($newtxns) > 0) {
                        foreach($newtxns as $newtxn) {
                            $content.= '<tr><td>'.$newtxn->transaction_date.'</td><td>'.$newtxn->transaction_memo . '</td><td>' . ($newtxn->account_from_id > 0 ? '-' : '').$newtxn->transaction_amount . '</td></tr>';
                            $amount += ($newtxn->transaction_amount * ($newtxn->account_from_id > 0 ? -1 : 1));
                        }
                    }
                }
            }

            $txns[] = (object)array(
                'transactiontype_colour' => $type->transactiontype_colour,
                'transactiontype_name' => $type->transactiontype_name,
                'transactiontype_id' => $type->transactiontype_id,
                'transaction_amount' => $amount
            );
        }

        $amount = 0;
        $newtxns = DB::table('transactions')
            ->where('transactions.account_from_id', '>', 0)
            ->whereNull('transactions.transactiontype_id')
            ->where('transactions.transaction_date', '>=', $startdate)
            ->where('transactions.transaction_date', '<=', $enddate)
            ->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
            ->get();
        if(count($newtxns) > 0) {
            $amount += $newtxns[0]->transaction_amount;
        }

        $txns[] = (object)array(
            'transactiontype_colour' => '#000000',
            'transactiontype_name' => 'Other',
            'transactiontype_id' => 0,
            'transaction_amount' => $amount
        );

        $content .= '</table>';
        return view('reports.transactiontypelist', ['content' => $content, 'title' => 'All transactions']);
    }

    public function transactionsByParent($parent_id) {
        $ttc = new TransactionTypesController();
        $tran = $ttc->getTransactionType($parent_id);
        $txns = array();

        $midLevel = $ttc->getTransactionTypesByParent($parent_id);
        foreach($midLevel as $childType) {
            $amount = 0;

            $newtxns = DB::table('transactions')
                //->where('transactions.account_from_id', '>', 0)
                ->where('transactions.transactiontype_id', '=', $childType->transactiontype_id)
                ->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                ->get();
            if(count($newtxns) > 0) {
                $amount += $newtxns[0]->transaction_amount;
            }

            $lowLevel = $ttc->getTransactionTypesByParent($childType->transactiontype_id);

            foreach($lowLevel as $grandChildType) {
                $newtxns = DB::table('transactions')
                    //->where('transactions.account_from_id', '>', 0)
                    ->where('transactions.transactiontype_id', '=', $grandChildType->transactiontype_id)
                    ->select(DB::raw('sum(transactions.transaction_amount) as transaction_amount'))
                    ->get();
                if(count($newtxns) > 0) {
                    $amount += $newtxns[0]->transaction_amount;
                }
            }

            $txns[] = (object)array(
                'transactiontype_colour' => $childType->transactiontype_colour,
                'transactiontype_name' => $childType->transactiontype_name,
                'transactiontype_id' => $childType->transactiontype_id,
                'transaction_amount' => $amount
            );
        }

        return view('reports.transactiontype', ['transactions' => $txns, 'title' => $tran->transactiontype_name]);
    }

    public function transactionsNoCategory() {
        $user = Auth::user();
        if(isset($_GET['startdate']) && isset($_GET['enddate'])) {
            $startdate = date('Y-m-d', strtotime($_GET['start']));
            $enddate = date('Y-m-d H:i:s', strtotime($_GET['end']));
        } else {
            if((int)date('m') < 9) {
                //last FY
                $startdate = ((int)date('Y') - 1).'-07-01';
                $enddate = ((int)date('Y')).'-06-30';
            } else {
                //this FY
                $startdate = ((int)date('Y')).'-07-01';
                $enddate = ((int)date('Y') + 1).'-06-30';
            }
        }
        $transactions = DB::table('transactions')
            ->leftJoin('accounts as ac_from', 'ac_from.account_id', '=', 'transactions.account_from_id')
            ->leftJoin('accounts as ac_to', 'ac_to.account_id', '=', 'transactions.account_to_id')
            ->whereNull('transactiontype_id')
            ->where('transactions.users_id', '=', $user->id)
            ->where('transactions.transaction_date', '>=', $startdate)
            ->where('transactions.transaction_date', '<=', $enddate)
            ->select('transactions.*', 'ac_to.account_name as to_account_name', 'ac_from.account_name as from_account_name')
            ->orderBy('transactions.transaction_date', 'asc')
            ->get();

        return view('reports.nocattransaction', ['transactions' => $transactions]);
    }
}
