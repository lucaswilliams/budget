<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

use App\Http\Requests;

class TransactionsController extends Controller
{
    public function getList() {
        $transactions = $this->getTransactions();
        return view('transactions.list', ['transactions' => $transactions]);
    }

    public function getListAccount($account_id) {
        $transactions = $this->getTransactions(null, $account_id);
        return view('transactions.list', ['transactions' => $transactions]);
    }

    private function getTransactions($batch_id = null, $account_id = null) {
        $user = Auth::user();
        $transactionQ = DB::table('transactions')
            ->leftJoin('accounts as ac_from', 'ac_from.account_id', '=', 'transactions.account_from_id')
            ->leftJoin('accounts as ac_to', 'ac_to.account_id', '=', 'transactions.account_to_id')
            ->leftJoin('transactiontypes', 'transactiontypes.transactiontype_id', '=', 'transactions.transactiontype_id')
            ->where('transactions.users_id', '=', $user->id);

        if($batch_id !== null) {
            $transactionQ->where('batch_id', '=', $batch_id);
        }

        if($batch_id !== null) {
            $transactionQ->where(function($query) use ($account_id) {
                $query->where('account_from_id', '=', $account_id)
                    ->orWhere('account_to_id', '=', $account_id);
            });
        }

        $transactionQ->select('transactions.*', 'ac_to.account_name as to_account_name', 'ac_from.account_name as from_account_name', 'transactiontypes.transactiontype_name')
            ->orderBy('transactions.transaction_date')
            ->orderBy('transactions.transaction_bank_id')
            ->orderBy('transactions.transaction_id');

        return $transactionQ->get();
    }

    public function addForm() {
        $ac = new AccountsController();
        $accounts = $ac->getAccounts();

        $ttc = new TransactionTypesController();
        $types = $ttc->getNestedTransactionTypes();

        return View('transactions.add', ['accounts' => $accounts, 'types' => $types]);
    }

    public function addFormSave(Request $request) {
        $user = Auth::user();
        DB::table('transactions')
            ->insert([
                'transaction_amount' => str_replace(",", "", $request->transaction_amount),
                'transaction_memo' => $request->transaction_memo,
                'transaction_date' => $request->transaction_date,
                'account_from_id' => $request->account_from_id,
                'account_to_id' => $request->account_to_id,
                'transactiontype_id' => $request->transactiontype_id,
                'users_id' => $user->id
            ]);

        return redirect('/transactions')->with('success', 'Your transaction was updated successfully');
    }

    public function editForm($transaction_id) {
        $transactions = DB::table('transactions')
            ->where('transaction_id', '=', $transaction_id)
            ->get();

        if(count($transactions) == 0) {
            return view('errors.404', ['message' => 'The transaction you\'re looking for cannot be found.']);
        } else {
            $ac = new AccountsController();
            $accounts = $ac->getAccounts();

            $ttc = new TransactionTypesController();
            $types = $ttc->getNestedTransactionTypes();

            $transaction = $transactions[0];
            return view('transactions.edit', ['transaction' => $transaction, 'accounts' => $accounts, 'types' => $types]);
        }
    }

    public function editFormSave(Request $request) {
        DB::table('transactions')
            ->where('transaction_id', '=', $request->transaction_id)
            ->update([
                'transaction_amount' => str_replace(",", "", $request->transaction_amount),
                'transaction_memo' => $request->transaction_memo,
                'transaction_date' => $request->transaction_date,
                'account_from_id' => $request->account_from_id,
                'account_to_id' => $request->account_to_id,
                'transactiontype_id' => $request->transactiontype_id
            ]);

        if($request->from == 0) {
            return redirect('/transactions')->with('success', 'Your transaction was updated successfully');
        }

        if($request->from == 1) {
            return redirect('/reports/transactionCat')->with('success', 'Your transaction was updated successfully');
        }
    }

    public function delete($transaction_id) {
        DB::table('transactions')
            ->where('transaction_id', '=', $transaction_id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function truncate() {
        $user = Auth::user();
        DB::table('transactions')
            ->where('users_id', '=', $user->id)
            ->delete();

        DB::table('batches')
            ->where('users_id', '=', $user->id)
            ->delete();

        return redirect($_SERVER['HTTP_REFERER']);
    }

    public function importForm() {
        $ac = new AccountsController();
        $accounts = $ac->getAccounts();
        return view('transactions.import', ['accounts' => $accounts]);
    }

    public function importFormConfirm(Request $request) {
        //This form needs to take the data from the previous page, re-display it, provide any of the transactiontype mappings (so they can be edited) and

        //open the file
        $name = $request->file('importFile')->getClientOriginalName();
        $temp = env('TEMP_PATH');
        $request->file('importFile')->move($temp, $name);

        $contents = file_get_contents($temp.'/'.$name);
        $contents = str_replace("<FITID>\r\n", "<FITID>NULL\r\n", $contents);
        $contents = str_replace(",", "", $contents);
        file_put_contents($temp.'/'.$name, $contents);

        $OfxParser = new \OfxParser\Parser;
        $Ofx = $OfxParser->loadFromFile($temp.'/'.$name);

        // Get the statements for the current bank account
        $transactions = $Ofx->BankAccount->Statement->transactions;

        $ttc = new TransactionTypesController();

        foreach($transactions as &$transaction) {
            $transaction_id = 0;

            $transaction->amount = abs($transaction->amount);

            if($transaction->uniqueId == "NULL") {
                $transaction->uniqueId = null;
            }

            if ($transaction->type == 'DEBIT') {
                $transaction->account_from_id = $request->account_id;
            } else {
                $transaction->account_to_id = $request->account_id;
            }

            $categories = $ttc->getTransactionTypesBySearch($transaction->memo);
            if(count($categories)) {
                $transaction->transactiontype_id = $categories[0]->transactiontype_id;
            }
        }

        usort($transactions, array($this, "sortTransactionDate"));

        $transactiontypes = $ttc->getNestedTransactionTypes();

        return view('transactions.importconfirm', ['transactions' => $transactions, 'transactiontypes' => $transactiontypes]);
    }

    private static function sortTransactionDate($a, $b) {
        $aDate = strtotime($a->date->format('Y-m-d'));
        $bDate = strtotime($b->date->format('Y-m-d'));
        if($aDate == $bDate) {
            return 0;
        }

        return ($aDate < $bDate) ? -1 : 1;
    }

    public function importFormSave(Request $request) {
        $user = Auth::user();

        $bc = new BatchesController();
        $batch_id = $bc->newBatch();

        foreach($request->transaction_amount as $idx => $amount) {
            $transaction_id = 0;

            if($request->uniqueId[$idx] != "") {
                $rec = DB::table('transactions')
                    ->where('users_id', '=', $user->id)
                    ->where('transaction_bank_id', '=', $request->uniqueId[$idx])
                    ->select('transaction_id')
                    ->get();

                if (count($rec) > 0) {
                    $r = $rec[0];
                    $transaction_id = $r->transaction_id;
                }
            }

            if($transaction_id > 0) {
                if ($request->account_from_id[$idx] > 0) {
                    $fields['account_from_id'] = $request->account_from_id[$idx];
                } else {
                    $fields['account_to_id'] = $request->account_to_id[$idx];
                }

                DB::table('transactions')
                    ->where('transaction_id', '=', $transaction_id)
                    ->update($fields);

            } else {
                $fields = array(
                    'users_id' => $user->id,
                    'transaction_bank_id' => $request->uniqueId[$idx],
                    'transaction_amount' => str_replace(",", "", $request->transaction_amount[$idx]),
                    'transaction_memo' => $request->transaction_memo[$idx],
                    'transaction_date' => $request->transaction_date[$idx],
                    'batch_id' => $batch_id,
                    'transactiontype_id' => ($request->transactiontype_id[$idx] > 0 ? $request->transactiontype_id[$idx] : null),
                    'account_from_id' => $request->account_from_id[$idx],
                    'account_to_id' => $request->account_to_id[$idx]
                );

                DB::table('transactions')
                    ->insert($fields);
            }
        }

        return view('transactions.importdone');
    }
}