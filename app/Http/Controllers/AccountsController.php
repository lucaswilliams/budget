<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

use App\Http\Requests;

class AccountsController extends Controller
{
    public function getAccounts() {
        $user = Auth::user();

        return DB::table('accounts')
            ->where('users_id', '=', $user->id)
            ->orderBy('account_name', 'asc')
            ->get();
    }

    public function getList() {
        $accounts = $this->getAccounts();

        return view('accounts.list', ['accounts' => $accounts]);
    }

    public function addForm() {
        return view('accounts.add');
    }

    public function addFormSave(Request $request) {
        $user = Auth::user();
        DB::table('accounts')
            ->insert([
                'users_id' => $user->id,
                'account_name' => $request->account_name
            ]);

        return redirect('/accounts');
    }

    public function editForm($account_id) {
        $user = Auth::user();
        $accounts = DB::table('accounts')
            ->where('account_id', '=', $account_id)
            ->where('users_id', '=', $user->id)
            ->get();

        if(count($accounts) > 0) {
            $account = $accounts[0];
            return view('accounts.edit', ['account' => $account]);
        } else {
            return view('errors.404', ['message' => 'The account you\'re looking for cannot be found.']);
        }
    }

    public function editFormSave(Request $request) {
        DB::table('accounts')
            ->where('account_id', '=', $request->account_id)
            ->update([
                'account_name' => $request->account_name
            ]);

        return redirect('/accounts');
    }

    public function delete($account_id) {
        DB::table('transactions')
            ->where('account_from_id', '=', $account_id)
            ->delete();

        DB::table('accounts')
            ->where('account_id', '=', $account_id)
            ->delete();

        return redirect('/accounts');
    }
}
