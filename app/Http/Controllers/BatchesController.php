<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

use App\Http\Requests;

class BatchesController extends Controller
{
    public function newBatch() {
        $user = Auth::user();

        $batch_id = DB::table('batches')
            ->insertGetId([
                'users_id' => $user->id,
                'batch_entered' => date('Y-m-d H:i:s')
            ]);

        return $batch_id;
    }
}
