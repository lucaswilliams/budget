<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/accounts', 'AccountsController@getList');
    Route::get('/accounts/add', 'AccountsController@addForm');
    Route::post('/accounts/add', 'AccountsController@addFormSave');
    Route::get('/accounts/edit/{id}', 'AccountsController@editForm')->where('id', '[0-9]+');
    Route::post('/accounts/edit/{id}', 'AccountsController@editFormSave')->where('id', '[0-9]+');
    Route::get('/accounts/delete/{id}', 'AccountsController@delete')->where('id', '[0-9]+');

    Route::get('/transactions', 'TransactionsController@getList');
    Route::get('/transactions/add', 'TransactionsController@addForm');
    Route::post('/transactions/add', 'TransactionsController@addFormSave');
    Route::get('/transactions/edit/{id}', 'TransactionsController@editForm')->where('id', '[0-9]+');
    Route::post('/transactions/edit/{id}', 'TransactionsController@editFormSave')->where('id', '[0-9]+');
    Route::get('/transactions/delete/{id}', 'TransactionsController@delete')->where('id', '[0-9]+');

    Route::get('/transactions/import', 'TransactionsController@importForm');
    Route::post('/transactions/import', 'TransactionsController@importFormConfirm');
    Route::post('/transactions/import/save', 'TransactionsController@importFormSave');

    Route::get('/transactions/truncate', 'TransactionsController@truncate');

    Route::get('/transactiontypes', 'TransactionTypesController@getList');
    Route::get('/transactiontypes/add', 'TransactionTypesController@addForm');
    Route::post('/transactiontypes/add', 'TransactionTypesController@addFormSave');
    Route::get('/transactiontypes/edit/{id}', 'TransactionTypesController@editForm')->where('id', '[0-9]+');
    Route::post('/transactiontypes/edit/{id}', 'TransactionTypesController@editFormSave')->where('id', '[0-9]+');
    Route::get('/transactiontypes/delete/{id}', 'TransactionTypesController@delete')->where('id', '[0-9]+');

    Route::get('/reports', 'ReportsController@showList');
    Route::get('/reports/transactionType', 'ReportsController@transactionsByType');
    Route::get('/reports/transactionTypeList', 'ReportsController@transactionsListByType');
    Route::get('/reports/transactionType/{id}', 'ReportsController@transactionsByParent')->where('id', '[0-9]+');
    Route::get('/reports/transactionCat', 'ReportsController@transactionsNoCategory');
});