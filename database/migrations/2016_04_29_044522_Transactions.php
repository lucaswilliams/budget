<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function(Blueprint $table) {
            $table->increments('account_id');
            $table->string('account_name', 50);
            $table->integer('users_id');
        });

        Schema::create('transactiontypes', function(Blueprint $table)  {
            $table->increments('transactiontype_id');
            $table->string('transactiontype_name', 50);
            $table->integer('users_id');
        });

        Schema::create('transactions', function(Blueprint $table) {
            $table->increments('transaction_id');
            $table->integer('users_id');
            $table->string('transaction_bank_id', 50)->nullable();
            $table->decimal('transaction_amount', 8, 2);
            $table->integer('account_from_id')->nullable();
            $table->integer('account_to_id')->nullable();
            $table->integer('transactiontype_id')->nullable();
            $table->string('transaction_memo', 200);
            $table->date('transaction_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
        Schema::drop('transactiontypes');
        Schema::drop('accounts');
    }
}
