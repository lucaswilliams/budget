<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Batches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batches', function(Blueprint $table) {
            $table->increments('batch_id');
            $table->integer('users_id');
            $table->datetime('batch_entered');
        });

        Schema::table('transactions', function($table) {
            $table->integer('batch_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('batches');
        Schema::table('transactions', function($table) {
            $table->dropColumn('batch_id');
        });
    }
}
