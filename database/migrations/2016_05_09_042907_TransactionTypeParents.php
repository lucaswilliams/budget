<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionTypeParents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
         * Transaction Types need to have parents, e.g
         * - Telecommunications
         *   - Phone
         *   - Internet
         * - Software Subscriptions
         *   - Office 365
         *   - Adobe
         *   - WoW
         *   - Google Music
         * - Living Expenses
         *   - Rent
         *   - Power
         *   - Food
         *     - Groceries
         *     - Take Away
         *     - Dining Out
         */

        //Transaction types also need to have a colour associated with them for graphing.
        Schema::table('transactiontypes', function($table) {
            $table->string('transactiontype_colour', 7);
            $table->integer('transactiontype_parent_id')->nullable();
        });

        /**
         * Transaction types will have maps in them too.
         * This will allow transactions to be assigned a category, for example
         * 'Wdl ATM' might be linked to the 'Cash Withdrawal' type, so anything with that in the description is assigned.
         */
        Schema::create('transactiontype_map', function(Blueprint $table) {
            $table->increments('map_id');
            $table->integer('transactiontype_id');
            $table->string('map_search', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactiontypes', function($table) {
            $table->dropColumn('transactiontype_colour');
            $table->dropColumn('transactiontype_parent_id');
        });

        Schema::drop('transactiontype_map');
    }
}
