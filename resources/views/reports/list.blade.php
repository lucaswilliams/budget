<?php $page_title = 'Transactions by Type Report'; ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Reports list</h1>
                <?php
                    if((int)date('m') < 9) {
                        //last FY
                        $start = ((int)date('Y') - 1).'-07-01';
                        $end = ((int)date('Y')).'-06-30';
                    } else {
                        //this FY
                        $start = ((int)date('Y')).'-07-01';
                        $end = ((int)date('Y') + 1).'-06-30';
                    }
                ?>
                <input type="date" id="startdate" value="<?php echo $start; ?>">
                <input type="date" id="enddate" value="<?php echo $end; ?>">
                <ul>
                    <li><a href="{{URL::to('/reports/transactionType')}}" class="report">Transactions By Type</a></li>
                    <li><a href="{{URL::to('/reports/transactionTypeList')}}" class="report">Transaction List By Type</a></li>
                    <li><a href="{{URL::to('/reports/transactionCat')}}" class="report">Transactions with no category</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection