<?php $page_title = 'Transactions by Type Report'; ?>

@extends('layouts.app')

@section('scripts')
<script type="text/javascript" src="{{ asset('/js/Chart.js') }}"></script>

<script>
    var ctx = $("#myChart");
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [
                <?php foreach($transactions as $transaction) {
                    if($transaction->transactiontype_name != "Income") {
                        echo '"'.$transaction->transactiontype_name.'",';
                    }
                } ?>
            ],
            datasets: [
            {
                data: [
                    <?php foreach($transactions as $transaction) {
                        if($transaction->transactiontype_name != "Income") {
                            echo '"'.abs($transaction->transaction_amount).'",';
                        }
                    } ?>
                ],
                backgroundColor: [
                    <?php foreach($transactions as $transaction) {
                        if($transaction->transactiontype_name != "Income") {
                            echo '"'.((strlen($transaction->transactiontype_colour) > 7) ? '#000000' : $transaction->transactiontype_colour).'",';
                        }
                    } ?>
                ]
            }]
        }
    });
</script>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1><?php echo $title; ?></h1>
                <canvas id="myChart"></canvas>
            </div>
            <div class="col-sm-6">
                <table class="table table-striped">
                    <tr>
                        <th colspan="2">Category</th>
                        <th>Amount</th>
                    </tr>
                    <?php foreach($transactions as $transaction) {
                        echo '<tr><td style="background-color: '.((strlen($transaction->transactiontype_colour) > 7) ? '#000000' : $transaction->transactiontype_colour).'"></td>';
                        echo '<td><a href="'.url('/reports/transactionType').'/'.$transaction->transactiontype_id.'">'.$transaction->transactiontype_name.'</a></td>';
                        echo '<td class="text-right">$'.number_format($transaction->transaction_amount,2).'</td></tr>';
                    } ?>
                </table>
            </div>
        </div>
    </div>
@stop