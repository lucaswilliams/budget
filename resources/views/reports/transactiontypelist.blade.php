<?php $page_title = 'Transactions by Type Report'; ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php echo $content; ?>
            </div>
        </div>
    </div>
@stop