<?php $page_title = 'Import Transactions'; ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Import transactions</h1>
                <form method="POST" enctype="multipart/form-data" action="import/save">
                    {{ csrf_field() }}
                    <table class="table table-striped">
                        <tr>
                            <th width="120px">Date</th>
                            <th>Description</th>
                            <th width="100px">Amount</th>
                            <th width="300px">Category</th>
                        </tr>
                        <?php foreach($transactions as $transaction) { ?>
                        <tr<?php echo ($transaction->transactiontype_id > 0 ? '' : ' class="danger"') ?>>
                            <td>
                                <input type="hidden" name="account_to_id[]" value="<?php echo $transaction->account_to_id; ?>">
                                <input type="hidden" name="account_from_id[]" value="<?php echo $transaction->account_from_id; ?>">
                                <input type="hidden" name="unique_id[]" value="<?php echo $transaction->uniqueId; ?>">
                                <input type="date" name="transaction_date[]" value="<?php echo $transaction->date->format('Y-m-d'); ?>" class="form-control"></td>
                            <td>
                                <input type="text" name="transaction_memo[]" value="<?php echo $transaction->memo; ?>" class="form-control">
                            </td>
                            <td class="text-right">
                                <input type="text" name="transaction_amount[]" value="<?php echo number_format($transaction->amount, 2); ?>" class="form-control">
                            </td>
                            <td>
                                <select name="transactiontype_id[]" class="form-control">
                                    <option value=""></option>
                                    <?php foreach($transactiontypes as $type) {
                                        $selected = '';
                                        if($type->transactiontype_id == $transaction->transactiontype_id) {
                                            $selected = ' selected';
                                        }
                                        echo '<option value="'.$type->transactiontype_id.'"'.$selected.'>'.$type->transactiontype_name.'</option>';
                                    } ?>)
                                </select>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Process</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop