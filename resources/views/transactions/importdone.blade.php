<?php $page_title = 'Import Complete'; ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Import complete</h1>
                <p>Your transactions have been imported successfully</p>
                <p>
                    <a class="btn btn-warning" href="{{ url('/transactions/import') }}"><i class="fa fa-cloud-upload"></i> Upload More</a>
                    <a class="btn btn-primary" href="{{ url('/transactions') }}"><i class="fa fa-list"></i> Back to list</a>
                </p>
            </div>
        </div>
    </div>
@stop