<?php $page_title = 'Edit Transation'; ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Edit Transaction</h1>
                <form method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="transaction_id" value="<?php echo $transaction->transaction_id; ?>">
                    <input type="hidden" name="from" value="<?php echo $_GET['from']; ?>">
                    <div class="form-group">
                        <label for="transaction_memo">Transaction Memo:</label>
                        <input type="text" name="transaction_memo" id="transaction_memo" class="form-control" value="<?php echo $transaction->transaction_memo; ?>">
                    </div>

                    <div class="form-group">
                        <label for="transaction_amount">Transaction Amount:</label>
                        <input type="text" name="transaction_amount" id="transaction_amount" class="form-control" value="<?php echo $transaction->transaction_amount; ?>">
                    </div>

                    <div class="form-group">
                        <label for="transaction_date">Transaction Date:</label>
                        <input type="date" name="transaction_date" id="transaction_date" class="form-control" value="<?php echo $transaction->transaction_date; ?>">
                    </div>

                    <div class="form-group">
                        <label for="account_from_id">From Account:</label>
                        <select name="account_from_id" id="account_from_id" class="form-control">
                            <option value=""></option>
                            <?php foreach($accounts as $account) {
                                $selected = '';
                                if($account->account_id == $transaction->account_from_id) {
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$account->account_id.'"'.$selected.'>'.$account->account_name.'</option>';
                            } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="account_to_id">To Account:</label>
                        <select name="account_to_id" id="account_to_id" class="form-control">
                            <option value=""></option>
                            <?php foreach($accounts as $account) {
                                $selected = '';
                                if($account->account_id == $transaction->account_to_id) {
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$account->account_id.'"'.$selected.'>'.$account->account_name.'</option>';
                            } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="transactiontype_id">Category:</label>
                        <select name="transactiontype_id" id="transactiontype_id" class="form-control">
                            <option value=""></option>
                            <?php foreach($types as $type) {
                                $selected = '';
                                if($type->transactiontype_id == $transaction->transactiontype_id) {
                                    $selected = ' selected';
                                }
                                echo '<option value="'.$type->transactiontype_id.'"'.$selected.'>'.$type->transactiontype_name.'</option>';
                            } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop