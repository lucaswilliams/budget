<?php $page_title = 'Transactions'; ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>My Transactions</h1>
            </div>
            <div class="col-xs-12 text-right">
                <a href="{{URL::to('/transactions/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add new</a>
                <a href="{{URL::to('/transactions/import')}}" class="btn btn-warning"><i class="fa fa-cloud-upload"></i> Import</a>
                <a href="{{URL::to('/transactions/truncate')}}" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
            </div>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Date</th>
                            <th>From Account</th>
                            <th>To Account</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Amount</th>
                            <th>Actions</th>
                        </tr>
                        <?php foreach($transactions as $transaction) { ?>
                        <tr>
                            <td><?php echo $transaction->transaction_date; ?></td>
                            <td><?php echo $transaction->from_account_name; ?></td>
                            <td><?php echo $transaction->to_account_name; ?></td>
                            <td><?php echo $transaction->transaction_memo; ?></td>
                            <td><?php echo $transaction->transactiontype_name; ?></td>
                            <td class="text-right"><?php echo number_format($transaction->transaction_amount, 2); ?></td>
                            <td>
                                <a href="{{ URL::to('/transactions/edit') }}/<?php echo $transaction->transaction_id; ?>?from=0" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                <a href="{{ URL::to('/transactions/delete') }}/<?php echo $transaction->transaction_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
