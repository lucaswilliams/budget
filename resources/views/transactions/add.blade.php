<?php $page_title = 'Add Transation'; ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Add Transaction</h1>
                <form method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="transaction_id">
                    <div class="form-group">
                        <label for="transaction_memo">Transaction Memo:</label>
                        <input type="text" name="transaction_memo" id="transaction_memo" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="transaction_amount">Transaction Amount:</label>
                        <input type="text" name="transaction_amount" id="transaction_amount" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="transaction_date">Transaction Date:</label>
                        <input type="date" name="transaction_date" id="transaction_date" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="account_from_id">From Account:</label>
                        <select name="account_from_id" id="account_from_id" class="form-control">
                            <option value=""></option>
                            <?php foreach($accounts as $account) {
                                echo '<option value="'.$account->account_id.'">'.$account->account_name.'</option>';
                            } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="account_to_id">To Account:</label>
                        <select name="account_to_id" id="account_to_id" class="form-control">
                            <option value=""></option>
                            <?php foreach($accounts as $account) {
                                echo '<option value="'.$account->account_id.'">'.$account->account_name.'</option>';
                            } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="transactiontype_id">Category:</label>
                        <select name="transactiontype_id" id="transactiontype_id" class="form-control">
                            <option value=""></option>
                            <?php foreach($types as $type) {
                                echo '<option value="'.$type->transactiontype_id.'">'.$type->transactiontype_name.'</option>';
                            } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop