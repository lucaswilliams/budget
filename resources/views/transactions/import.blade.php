<?php $page_title = 'Import Transactions'; ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Add account</h1>
                <form method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="account_id">Account:</label>
                        <select name="account_id" id="account_id" class="form-control">
                            <?php foreach($accounts as $account) { ?>
                                <option value="<?php echo $account->account_id; ?>"><?php echo $account->account_name; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="importFile">File</label>
                        <input type="file" name="importFile" id="importFile" class="form-control">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-cloud-upload"></i> Upload</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop