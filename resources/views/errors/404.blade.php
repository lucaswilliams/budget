<?php $page_title = 'Error'; ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>404</h1>
                <p><?php echo $message; ?></p>
                <p>Please head back to where you were and try again.</p>
            </div>
        </div>
    </div>
@stop