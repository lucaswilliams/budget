<?php $page_title = 'Accounts'; ?>
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>My Accounts</h1>
        </div>
        <div class="col-xs-12 text-right">
            <a href="{{URL::to('/accounts/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i>Add new</a>
        </div>
        <div class="col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Account Name</th>
                        <th>Actions</th>
                    </tr>
                    <?php foreach($accounts as $account) { ?>
                        <tr>
                            <td><?php echo $account->account_name; ?></td>
                            <td>
                                <a href="{{ URL::to('/accounts/edit') }}/<?php echo $account->account_id; ?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                <a href="{{ URL::to('/accounts/delete') }}/<?php echo $account->account_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
