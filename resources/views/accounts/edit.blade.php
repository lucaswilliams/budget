<?php $page_title = 'Edit Account'; ?>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Edit Account</h1>
                <form method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="account_id" value="<?php echo $account->account_id; ?>">
                    <div class="form-group">
                        <label for="account_name">Account Name:</label>
                        <input type="text" name="account_name" id="account_name" class="form-control" value="<?php echo $account->account_name; ?>">
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop