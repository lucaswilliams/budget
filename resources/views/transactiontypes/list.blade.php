<?php $page_title = 'Transaction Types'; ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>My Transaction Types</h1>
            </div>
            <div class="col-xs-12 text-right">
                <a href="{{URL::to('/transactiontypes/add')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Add new</a>
            </div>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th colspan="2">Name</th>
                            <th>Actions</th>
                        </tr>
                        <?php foreach($transactiontypes as $transactiontype) { ?>
                        <tr>
                            <td style="background-color: <?php echo $transactiontype->transactiontype_colour; ?>;"></td>
                            <td><?php echo $transactiontype->transactiontype_name; ?></td>
                            <td>
                                <a href="{{ URL::to('/transactiontypes/edit') }}/<?php echo $transactiontype->transactiontype_id; ?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                                <a href="{{ URL::to('/transactiontypes/delete') }}/<?php echo $transactiontype->transactiontype_id; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
