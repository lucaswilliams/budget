<?php $page_title = 'Add Transaction Type'; ?>

@extends('layouts.app')

@section('scripts')
    <script type="text/javascript">
        $('#addMapRow').click(function(e) {
            $row = $('#templateRow').clone().removeAttr('id');
            $.each($row.find('input'), function(k, v) {
                $(v).removeAttr('disabled');
            });

            $row.insertBefore('#addRow');
        });
    </script>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Add Transaction Type</h1>
                <form method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="transactiontype_name">Transaction Type Name:</label>
                        <input type="text" name="transactiontype_name" id="transactiontype_name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="transactiontype_colour">Colour (for graphs):</label>
                        <input type="color" name="transactiontype_colour" id="transactiontype_colour" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="transactiontype_parent_id">Parent:</label>
                        <select name="transactiontype_parent_id" id="transactiontype_parent_id" class="form-control">
                            <option value=""></option>
                            <?php if(count($types) > 0) { foreach($types as $type) {
                                echo '<option value="'.$type->transactiontype_id.'">'.$type->transactiontype_name.'</option>';
                            }} ?>
                        </select>
                    </div>

                    <table class="table table-striped">
                        <tr>
                            <th>Mapping String</th>
                            <th>Action</th>
                        </tr>

                        <tr id="addRow">
                            <th colspan="2" class="text-right">
                                <a class="btn btn-success btn-small" id="addMapRow"><i class="fa fa-plus"></i> Add</a>
                            </th>
                        </tr>
                    </table>

                    <table style="display: none">
                        <tr id="templateRow"><td><input type="text" name="map_search[]" class="form-control" disabled></td><td><input type="text" name="map_id[]" disabled></td></tr>
                    </table>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop